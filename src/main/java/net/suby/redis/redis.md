**● EC2에 redis 설치**
---
1. redis 공식가이드
https://redis.io/download
http://redis.io/topics/quickstart

2. redis 설치
~~~
$ wget http://download.redis.io/releases/redis-3.2.9.tar.gz
$ tar xzf redis-3.2.9.tar.gz
$ cd redis-3.2.9
$ make
~~~

make시 에러날 경우
이 프로세스가하는 일은 컴퓨터에서 실행 파일을 컴파일하고 생성 한 다음 설치하는 것입니다. 
이때 필요한 도구를 설치해 줘야 한다.
~~~
$ yum grouplist
$ sudo yum groupinstall 'Development Tools'
~~~

or
~~~
$ cd deps
$  make geohash-int
~~~

3. protected-mode
~~~
vi redis.conf
protected-mode no 로 변경
~~~

4. redis 서버 실행
~~~
$ src/redis-server
src/redis-server --protected-mode no
~~~

5. redis using
~~~
$ src/redis-cli
redis> set foo bar
OK
redis> get foo
"bar"
~~~

